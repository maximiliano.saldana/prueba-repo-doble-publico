#carga bibliotecas para cargar y manipular los datos
library(haven)
library(dplyr)
library(foreign)

#carga los datos, usar los directorios donde lso tengan

ENGIH_2016_Base_de_Datos_Gastos <- read_sav("C:/Users/Usuario/Desktop/proyectoeconometria/datos/ENGIH 2016 Base de Datos Gastos.sav")

ENGIH_2016_Base_de_Datos_Hogares <- read_sav("C:/Users/Usuario/Desktop/proyectoeconometria/datos/ENGIH 2016 Base de Datos Hogares.sav")

ENGIH_2016_Base_de_Datos_Personas <- read_sav("C:/Users/Usuario/Desktop/proyectoeconometria/datos_mod/ENGIH 2016 Base de Datos Personas.sav")


#filra los gastos que en nuestro casos nos interesan nos interesan
data_gastos_mod <- ENGIH_2016_Base_de_Datos_Gastos %>% 
  as.data.frame() %>% 
  filter(ARTICULOCODIGO %in% c("06111011","06111012","06111013","06111014","06111015","06111016","06111017","06111018", "06111020","06111030", "06112010", "06441010" ,"06441020")) %>% 
  group_by(NUMERO) %>% 
  summarise(gasto_mens = sum(VALORCONTM))

#mvd fijada como region de referencia
data_hog <- ENGIH_2016_Base_de_Datos_Hogares %>% 
  select(NUMERO, REGION) %>% 
  mutate(inte_grande = if_else(REGION == "02",1,0), inte_chico = if_else(REGION == "03",1,0)) %>% 
  select(-REGION)

#une los gastos con los datos de hogares
datos_conhog <- left_join(data_hog, data_gastos_mod, by=c("NUMERO" = "NUMERO"))
  
#crea variables de categorizacion para los hogares a partir de la info de las personas
base_personas <- ENGIH_2016_Base_de_Datos_Personas %>% 
         select(NUMERO, 
                sexo = E26, 
                edad = E27, 
                ingresos = PT1,
                msp = E45_1,
                iamc = E45_2, 
                smp = E45_3, 
                hpol.mil = E45_4, 
                bps = E45_5, 
                polmun = E45_6, 
                edu_prim = E197_1, 
                edu_med = E201_1, 
                edu_utu = E212_1, 
                edu_mag = E215_1, 
                edu_uni = E218_1,
                edu_ter = E221_1,
                edu_post = E224_1) %>% 
  mutate(
    mujer = ifelse(sexo == 2, 1, 0), 
    edad_mas65 = ifelse(edad >= 65, 1, 0),
    edad_menos3 = ifelse(edad <= 3, 1, 0),
    mujer_fert = ifelse(mujer == 1, ifelse(edad>=14 & edad <= 49,1,0) ,0 ),
    
    msp = if_else(msp == 1, 1, 0, missing = 0), 
    iamc = if_else(iamc == 1, 1, 0, missing = 0),
    smp = if_else(smp == 1, 1, 0, missing = 0),
    hpol.mil = if_else(hpol.mil == 1, 1, 0, missing = 0),
    bps = if_else(bps == 1, 1, 0, missing = 0),
    polmun = if_else(polmun == 1, 1, 0, missing = 0),
    
    salud_priv = if_else(iamc == 1 | smp == 1, 1, 0),
    
    edu_1 = if_else(edu_prim == 1, 1, 0, missing = 0),
    edu_2 = if_else(edu_med == 1 | edu_utu == 1, 1, 0, missing = 0),
    edu_3 = if_else((edu_ter == 1 | edu_mag == 1) | edu_uni == 1, 1, 0, missing = 0),
    edu_4 = if_else(edu_post == 1, 1, 0, missing = 0)
  )


base_anex <- base_personas %>% 
  group_by(NUMERO) %>% 
  summarise(
    n = n(),
    
    ingresos = sum(ingresos),
    
    prop_m = sum(mujer)/n(),
    prop_mfert = sum(mujer_fert)/n(),
    
    prop_65plus = sum(edad_mas65)/n(),
    prop_3menos = sum(edad_menos3)/n(),
    
    prop_salud_priv = sum(salud_priv)/n(),
    
    ed_max_pri = if_else(sum(edu_1) >= 1, if_else(sum(edu_2) == 0, 1 , 0), 0),
    ed_max_sec = if_else(sum(edu_2) >= 1, if_else(sum(edu_3) == 0, 1 , 0), 0),
    ed_max_ter = if_else(sum(edu_3) >= 1, if_else(sum(edu_4) == 0, 1 , 0), 0),
    ed_max_post = if_else(sum(edu_4) >= 1, 1, 0)
  )

#une las dos bases 
datos_meds <- left_join(datos_conhog, base_anex , by=c("NUMERO" = "NUMERO"))

#guarda los datos en formato stata
write.dta(datos_meds,  "C:/Users/Usuario/Desktop/proyectoeconometria/datos_mod/datos_meds.dta")













